import sys
import os
import urllib


if __name__ == "__main__":
    sys.path.append(os.path.dirname(os.path.dirname(
        os.path.realpath(__file__))))
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "field_control.settings")
    import django
    django.setup()


from django.core import serializers

from run_manager.models import Run, RunEvent
from field_control.settings import MASTER_URI


def synchronise_runs():
    runevent_data = serializers.serialize("json", RunEvent.objects.all())
    run_data = serializers.serialize("json", Run.objects.all())
    data = [('runevent_data', runevent_data),
            ('run_data', run_data),
            ('pass', 'aeouhcic}}p.pui')]  # temprorary solution
    post_data = bytes(urllib.parse.urlencode(data).encode())

    result = urllib.request.urlopen(MASTER_URI, post_data)

    content = result.read()
    print(content.decode())


def main():
    synchronise_runs()

if __name__ == "__main__":
    main()
