# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-26 16:22
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('events', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Competition',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=500)),
                ('conf_data', models.TextField(blank=True)),
                ('attraction', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='events.Attraction')),
            ],
        ),
        migrations.CreateModel(
            name='Run',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('comment', models.TextField(blank=True)),
                ('competition', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='run_manager.Competition')),
            ],
        ),
        migrations.CreateModel(
            name='RunEvent',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=100)),
                ('data', models.TextField(blank=True)),
                ('datetime', models.DateTimeField()),
                ('added_datetime', models.DateTimeField(auto_now_add=True)),
                ('valid', models.BooleanField(default=True)),
                ('competition', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='run_manager.Competition')),
                ('run', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='run_manager.Run')),
            ],
            options={
                'ordering': ['-datetime'],
            },
        ),
        migrations.AlterUniqueTogether(
            name='runevent',
            unique_together=set([('name', 'run')]),
        ),
    ]
