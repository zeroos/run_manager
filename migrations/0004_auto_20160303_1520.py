# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('run_manager', '0003_auto_20160303_1513'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='run',
            options={'ordering': ['-seq']},
        ),
    ]
