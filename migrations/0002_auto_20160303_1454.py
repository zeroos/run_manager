# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('run_manager', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='run',
            options={'ordering': ['-seq']},
        ),
        migrations.AddField(
            model_name='run',
            name='seq',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterUniqueTogether(
            name='run',
            unique_together=set([('competition', 'seq')]),
        ),
    ]
