# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('run_manager', '0004_auto_20160303_1520'),
    ]

    operations = [
        migrations.AddField(
            model_name='competition',
            name='competition_type',
            field=models.CharField(max_length=1, default='S', choices=[('S', 'sum'), ('B', 'best')]),
        ),
    ]
