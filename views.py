import csv

from django.core import serializers
from django.contrib.auth.decorators import permission_required
from django.db.models import Max
from django.db import transaction
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View
from django.views.generic.list import ListView
from django.views.generic.edit import DeleteView, CreateView, UpdateView
from django.views.generic.base import TemplateView
from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from datetime import datetime, timezone, timedelta
from django.http import HttpResponseRedirect
from django.views.generic.detail import SingleObjectTemplateResponseMixin
from django.views.generic.edit import ModelFormMixin, ProcessFormView


from run_manager.models import Run, Competition, RunEvent


class AppMixin:
    def get_template_names(self):
        templates = super().get_template_names()
        return [f.replace(".html", "_app.html") for f in templates]


@method_decorator(permission_required('run_manager.add_run'), name='dispatch')
class AddIdentity(SingleObjectTemplateResponseMixin, ModelFormMixin,
                  ProcessFormView):
    model = RunEvent
    fields = ['data']
    template_name = 'run_manager/add_identity.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(AddIdentity, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(AddIdentity, self).post(request, *args, **kwargs)

    def get_object(self, queryset=None):
        if 'run' not in self.kwargs:
            return None
        run = get_object_or_404(Run, id=self.kwargs['run'])
        identity = RunEvent.objects.filter(run=run, name='confirmed_id')
        if identity.exists():
            return identity.get()
        return None

    def get_success_url(self):
        competition = Competition.objects.latest('id')
        return '/run/{}'.format(str(competition.id))

    def get_run_seq(self, competition):
        try:
            max_seq = Run.objects.filter(competition=competition).aggregate(
                Max('seq'))
            seq = max_seq['seq__max'] + 1
        except TypeError:
            seq = 1
        return seq

    def form_valid(self, form):
        form.instance.name = 'confirmed_id'
        form.instance.datetime = datetime.now(timezone.utc)
        competition = Competition.objects.latest('id')
        form.instance.competition = competition
        if 'run' in self.kwargs:
            run = get_object_or_404(Run, id=self.kwargs['run'])
            # identity = RunEvent.objects.filter(run=run, name='confirmed_id')
        else:
            run = Run()
            run.competition = Competition.objects.latest('id')
            run.seq = self.get_run_seq(competition)
            run.save()
        form.instance.run = run
        return super(AddIdentity, self).form_valid(form)


@method_decorator(permission_required('run_manager.add_run'), name='dispatch')
class ResetRuns(View):
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        runs = Run.objects.filter(competition=kwargs['competition'])
        for r in runs:
            if r.has_time():
                continue
            if not r.is_dns():
                try:
                    if r.start is None:
                        re = RunEvent()
                        re.name = 'dns'
                        re.run = r
                        re.competition = r.competition
                        re.datetime = datetime.now()
                        re.save()
                except RunEvent.DoesNotExist:
                    re = RunEvent()
                    re.name = 'dns'
                    re.run = r
                    re.competition = r.competition
                    re.datetime = datetime.now()
                    re.save()
            if not r.is_dnf() and not r.is_dsq():
                try:
                    if r.finish is None:
                        re = RunEvent()
                        re.name = 'dnf'
                        re.run = r
                        re.competition = r.competition
                        re.datetime = datetime.now()
                        re.save()
                except RunEvent.DoesNotExist:
                    re = RunEvent()
                    re.name = 'dnf'
                    re.run = r
                    re.competition = r.competition
                    re.datetime = datetime.now()
                    re.save()
        return HttpResponseRedirect(
            reverse('run-list', args=(kwargs['competition'],)))


@method_decorator(permission_required('run_manager.add_run'), name='dispatch')
class AddRunEvent(CreateView):
    model = RunEvent
    fields = ['name']
    template_name = 'run_manager/add_identity.html'

    def get_success_url(self):
        competition = Competition.objects.latest('id')
        return '/run/{}'.format(str(competition.id))

    def form_valid(self, form):
        run = get_object_or_404(Run, id=self.kwargs['run'])
        competition = get_object_or_404(Competition, id=run.competition.id)
        form.instance.run = run
        form.instance.datetime = datetime.now(timezone.utc)
        form.instance.competition = competition
        return super(AddRunEvent, self).form_valid(form)


@method_decorator(permission_required('run_manager.add_run'), name='dispatch')
class RunEventDelete(DeleteView):
    model = RunEvent

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        self.object.valid = False
        self.object.run = None
        self.object.save()
        return HttpResponseRedirect(success_url)

    def get_success_url(self):
        competition = Competition.objects.latest('id')
        return '/run/{}/runevents'.format(str(competition.id))


@method_decorator(permission_required('run_manager.add_run'), name='dispatch')
class RunEventUpdate(UpdateView):
    model = RunEvent
    fields = ['run']
    template_name_suffix = '_update'

    def get_form(self):
        form = super(RunEventUpdate, self).get_form()
        competition = get_object_or_404(Competition,
                                        id=self.kwargs['competition'])
        form.fields['run'].queryset = Run.objects.filter(
            competition=competition)
        return form

    def get_success_url(self):
        return '/run/{}/runevents'.format(str(self.kwargs['competition']))


@method_decorator(permission_required('run_manager.add_run'), name='dispatch')
class RunEventList(ListView):
    def get_queryset(self):
        competition = get_object_or_404(Competition,
                                        id=self.kwargs['competition'])
        return RunEvent.objects.filter(competition=competition) \
            .prefetch_related("run")

    def get_context_data(self, **kwargs):
        competition = get_object_or_404(Competition,
                                        id=self.kwargs['competition'])
        context = super().get_context_data(**kwargs)
        context['competition'] = competition
        return context


@method_decorator(permission_required('run_manager.add_run'), name='dispatch')
class RunList(ListView):
    http_method_names = ['get']

    def get_queryset(self):
        self.competition = get_object_or_404(Competition, id=self.args[0])

        return Run.objects.filter(competition=self.competition)\
            .order_by("-seq") \
            .prefetch_related("runevent_set") \
            .prefetch_related("competition")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['competition'] = self.competition

        context['refresh'] = ('refresh' in self.request.GET)

        return context


class RunListApp(AppMixin, RunList):
    pass



@method_decorator(permission_required('run_manager.add_run'), name='dispatch')
class Runs2CSV(ListView):
    http_method_names = ['get']
    filename = "runs.csv"

    def get_queryset(self):
        self.competition = get_object_or_404(Competition,
                                             id=self.kwargs['competition'])

        return Run.objects.filter(competition=self.competition)

    def get(self, request, **kwargs):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename={}'.format(
            self.filename)

        runs = self.get_queryset()

        writer = csv.writer(response)
        writer.writerow(self.competition.displayed_runevents)
        for run in runs:
            writer.writerow(run.runevent_data)

        return response


class RunsTimer(TemplateView):
    template_name = 'run_manager/timer.html'

    def dispatch(self, *args, **kwargs):
        self.competition = get_object_or_404(Competition,
                                             id=self.kwargs["competition"])
        return super().dispatch(*args, **kwargs)

    def get_displayed_runs(self):
        result = []
        now = datetime.now(timezone.utc)
        last_finish = RunEvent.objects.filter(name="finish").latest("datetime")
        if now - last_finish.datetime < timedelta(seconds=120):
            result.append(last_finish.run)

        return result + list(Run.all_in_progress(self.competition))

    def get_timer_values(self):
        return map(lambda r: Run.format_time(r.running_time) if r.running_time is not None
                   else "start", self.get_displayed_runs())

    def get_identities(self):
        return map(lambda r: r.identity.data if r.identity is not None
                   else None, self.get_displayed_runs())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['runs'] = zip(self.get_identities(),
                              self.get_timer_values())
        context['competition'] = self.competition
        return context


class RunsTimerApp(AppMixin, RunsTimer):
    pass

class SynchroniseView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
                return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if request.POST.get("pass") != 'aeouhcic}}p.pui':
            return HttpResponse("Wrong password")
        run_data = request.POST['run_data']
        runevent_data = request.POST['runevent_data']
        result = "OK\n"

        with transaction.atomic():
            for o in serializers.deserialize("json", run_data):
                o.save()
                result += str(o) + "\n"
            for o in serializers.deserialize("json", runevent_data):
                # This is a little bit hacky, but it works. Without
                # unlinking all runevents from runs before actually
                # updating them there might be a unique constraint
                # violation when a runevent is unlinked from one run
                # and another runevent is linked to the same run
                o.object.run = None
                o.save()
            for o in serializers.deserialize("json", runevent_data):
                o.save()
                result += str(o) + "\n"

        return HttpResponse(result)
