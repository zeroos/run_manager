from django.conf.urls import url

from run_manager import views

urlpatterns = [
    url(r'^(\d+)$', views.RunList.as_view(),
        name='run-list'),
    url(r'^(?P<competition>\d+)/runevents$', views.RunEventList.as_view(),
        name='event-list'),
    url(r'^reset/(?P<competition>\d+)$', views.ResetRuns.as_view(),
        name='reset-runs'),
    url(r'^identity/add$', views.AddIdentity.as_view(),
        name='add-identity'),
    url(r'^identity/add/(?P<run>[^/]+)$', views.AddIdentity.as_view(),
        name='add-identity'),
    url(r'^runevent/add/(?P<run>[^/]+)$',
        views.AddRunEvent.as_view(),
        name='add-runevent'),
    url(r'^runevent/delete/(?P<pk>[^/]+)$', views.RunEventDelete.as_view(),
        name='delete-runevent'),
    url(r'^(?P<competition>\d+)/runevent/update/(?P<pk>[^/]+)$',
        views.RunEventUpdate.as_view(),
        name='update-runevent'),
    url(r'^(?P<competition>\d+)/csv$', views.Runs2CSV.as_view(),
        name='runs-csv'),
    url(r'^sync$', views.SynchroniseView.as_view(),
        name='runs-synchronise'),
    url(r'^(?P<competition>\d+)/timer$', views.RunsTimer.as_view(),
        name='runs-timer'),
    url(r'^(?P<competition>\d+)/timer/app$', views.RunsTimerApp.as_view(),
        name='runs-timer-app'),
    url(r'^(\d+)/app$', views.RunListApp.as_view(),
        name='run-list-app'),
]
