from datetime import timedelta
import serial
import logging
import uuid
import re

from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save
from django.dispatch import receiver

from django.conf import settings

from django.utils import timezone

from events.models import Attraction


class Competition(models.Model):
    TYPE_CHOICES = (
        ('S', 'sum'),
        ('B', 'best'),
    )
    name = models.CharField(max_length=500, blank=True)
    conf_data = models.TextField(blank=True)
    attraction = models.ForeignKey(Attraction, null=True, blank=True)
    competition_type = models.CharField(max_length=1, choices=TYPE_CHOICES,
                                        default='S')

    def __str__(self):
        return self.name

    @property
    def displayed_runevents(self):
        return ["start", "identity", "time"]

    @classmethod
    def get_active(cls):
        return cls.objects.latest("id")


class Run(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    competition = models.ForeignKey(Competition)
    comment = models.TextField(blank=True)
    seq = models.IntegerField(null=True)

    def get_runevent(self, name):
        for e in self.runevent_set.all():
            if e.name == name:
                return e
        raise RunEvent.DoesNotExist

    @property
    def runevent_names(self):
        """
        Returns a list of distinct events that occured in one of the runs,
        ordered by occurence time.
        """
        seen = set()
        result = [x.name for x in self.runevent_set.all()
                  if not (x.name in seen or seen.add(x.name))]
        if 'start' in seen and 'finish' in seen:
            result.append('time')

        return result

    @property
    def start(self):
        # return self.runevent_set.get(name='start')
        return self.get_runevent('start')

    @property
    def finish(self):
        try:
            # return self.runevent_set.get(name='finish')
            return self.get_runevent('finish')
        except RunEvent.DoesNotExist:
            return None

    @classmethod
    def all_in_progress(cls, competition):
        return cls.objects.filter(~Q(
            runevent__name__in=RunEvent.end_event_names() + ('time', )),
            runevent__name='start',
            competition=competition).order_by("seq")

    @staticmethod
    def clip_microseconds(t):
        micro = round(t.microseconds/10000)*10000
        return timedelta(t.days, t.seconds, micro)

    @property
    def running_time(self):
        if self.time is not None:
            return self.time
        else:
            return self.clip_microseconds(timezone.now() - self.start.datetime)

    @property
    def time(self):
        try:
            t_str = self.get_runevent('time').data
            d = re.match(r'(?P<m>\d+):(?P<s>\d+).(?P<cs>\d+)',
                         t_str).groupdict(0)
            cs = int(d['m']) * 60 * 100 + int(d['s']) * 100 + int(d['cs'])
            t = timedelta(microseconds=cs*10000)
        except RunEvent.DoesNotExist:
            try:
                if self.finish is None or self.start is None:
                    return None
                t = (self.finish.datetime -
                     self.start.datetime)
            except RunEvent.DoesNotExist:
                return None
        return self.clip_microseconds(t)

    @staticmethod
    def format_time(td):
        hours, remainder = divmod(td.seconds, 3600)
        minutes, seconds = divmod(remainder, 60)

        timestamp = "{0:01d}:{1:02d}.{2:02d}".format(
            minutes,
            seconds,
            int(td.microseconds/10000)
        )
        return timestamp

    @property
    def display_start(self):
        if self.is_dns():
            return 'DNS'
        else:
            try:
                # runevent = self.runevent_set.get(name='start')
                return self.start.datetime.strftime("%H:%M:%S")
            except RunEvent.DoesNotExist:
                return None

    @property
    def display_time(self):
        time = self.time
        if self.is_dsq():
            return 'DSQ ({})'.format(self.format_time(time))
        if self.is_dnf():
            return 'DNF'
        if time is None:
            return ''
        return self.format_time(time)

    def has_time(self):
        return False
        try:
            # if self.runevent_set.get(name='time'):
            if self.time:
                return True
        except RunEvent.DoesNotExist:
            return False

    def is_dnf(self):
        try:
            # if self.runevent_set.get(name='dnf'):
            if self.get_runevent('dnf'):
                return True
        except RunEvent.DoesNotExist:
            return False

    def is_dsq(self):
        try:
            # if self.runevent_set.get(name='dsq'):
            if self.get_runevent('dsq'):
                return True
        except RunEvent.DoesNotExist:
            return False

    def is_dns(self):
        try:
            # if self.runevent_set.get(name='dns'):
            if self.get_runevent('dns'):
                return True
        except RunEvent.DoesNotExist:
            return False

    @property
    def identity(self):
        try:
            # return self.runevent_set.get(name='confirmed_id')
            return self.get_runevent('confirmed_id')
        except RunEvent.DoesNotExist:
            try:
                # return self.runevent_set.get(name='scanned_id')
                return self.get_runevent('scanned_id')
            except RunEvent.DoesNotExist:
                return None

    @property
    def display_identity(self):
        identity = self.identity
        return identity.data

    @property
    def runevent_data(self):
        data = []

        for n in self.competition.displayed_runevents:
            if hasattr(self, 'display_{}'.format(n)):
                r = str(getattr(self, 'display_{}'.format(n)))
            else:
                try:
                    # runevent = self.runevent_set.get(name=n)
                    runevent = self.get_runevent(n)
                    r = runevent.datetime.strftime("%H:%M:%S")
                except RunEvent.DoesNotExist:
                    r = None
            data.append(r)

        return data

    def __str__(self):
        msg = "Run not started yet"
        try:
            if self.start:
                msg = "Run in progress since {}...".format(self.start.datetime)
            if self.finish:
                msg = "Run {} - {}".format(self.start.datetime,
                                           self.finish.datetime)
        except RunEvent.DoesNotExist:
            pass
        if self.identity is not None:
            msg = "#{} {}".format(self.identity.data, msg)
        return msg

    class Meta:
        unique_together = ("competition", "seq")
        ordering = ['seq']


class RunEvent(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=100)
    data = models.TextField(blank=True)
    datetime = models.DateTimeField()
    added_datetime = models.DateTimeField(auto_now_add=True)
    run = models.ForeignKey(Run, blank=True, null=True)
    competition = models.ForeignKey(Competition)
    valid = models.BooleanField(default=True, blank=True)

    def runevent_properties(self):
        properties = [self.id, self.name, self.datetime]
        try:
            properties.append(self.run.id)
        except AttributeError:
            properties.append(None)
        return properties

    def __str__(self):
        return "{}@{}".format(self.name, self.datetime)

    class Meta:
        unique_together = ("name", "run")
        ordering = ['-datetime']

    @staticmethod
    def end_event_names():
        return ('finish', 'dnf', 'dns')


@receiver(post_save, sender=RunEvent)
def run_event_sent(sender, **kwargs):
    runevent = kwargs['instance']
    if runevent.name == "finish" and \
            runevent.run is not None and \
            runevent.run.time is not None:
        try:
            for display in settings.TIME_DISPLAYS:
                try:
                    display.display_time(runevent.run.time)
                except serial.serialutil.SerialException:
                    logging.warning("Could not display time")
        except AttributeError:
            pass
