from django.contrib import admin
from run_manager.models import Competition, Run, RunEvent

admin.site.register(Competition)
admin.site.register(Run)
admin.site.register(RunEvent)
